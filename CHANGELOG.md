# [7.0.0](https://gitlab.com/study-gp/terragrunt-argocd/compare/v6.0.0...v7.0.0) (2022-02-28)


### Bug Fixes

* **ci:** install @semantic-release/exec in the CI ([93c58c8](https://gitlab.com/study-gp/terragrunt-argocd/commit/93c58c85f7b4b525b25edd46708135ec9d7820d2))
* **ci:** typo in .releaserc file ([abe2b4a](https://gitlab.com/study-gp/terragrunt-argocd/commit/abe2b4adb9ea48b584f368c7d3a343f50cd33e61))


### Documentation

* **pbi542295:** edit README ([e9c9e94](https://gitlab.com/study-gp/terragrunt-argocd/commit/e9c9e94570d832da9da9673a4d7fb4c733294f34))


### Features

* **pbi542295:** edit README ([1b9405a](https://gitlab.com/study-gp/terragrunt-argocd/commit/1b9405aed2bbc595ecb36602c679ce027f751784))


### BREAKING CHANGES

* **pbi542295:** + terraform version was changed from 0.12.29 to 1.0.10
+ implementing the possibility to use spot instances in EKS module
+ change in master MTLS Mode from Permissive to Strict
+ adding storage encrypted, and changing db engine to we
* **pbi542295:** + terraform version was changed from 0.12.29 to 1.0.10
+ implementing the possibility to use spot instances in EKS module
+ change in master MTLS Mode from Permissive to Strict
+ adding storage encrypted, and changing db engine to web

# [7.0.0-beta.1](https://gitlab.com/study-gp/terragrunt-argocd/compare/v6.0.0...v7.0.0-beta.1) (2022-02-28)


### Bug Fixes

* **ci:** install @semantic-release/exec in the CI ([93c58c8](https://gitlab.com/study-gp/terragrunt-argocd/commit/93c58c85f7b4b525b25edd46708135ec9d7820d2))
* **ci:** typo in .releaserc file ([abe2b4a](https://gitlab.com/study-gp/terragrunt-argocd/commit/abe2b4adb9ea48b584f368c7d3a343f50cd33e61))


### Documentation

* **pbi542295:** edit README ([e9c9e94](https://gitlab.com/study-gp/terragrunt-argocd/commit/e9c9e94570d832da9da9673a4d7fb4c733294f34))


### Features

* **pbi542295:** edit README ([1b9405a](https://gitlab.com/study-gp/terragrunt-argocd/commit/1b9405aed2bbc595ecb36602c679ce027f751784))


### BREAKING CHANGES

* **pbi542295:** + terraform version was changed from 0.12.29 to 1.0.10
+ implementing the possibility to use spot instances in EKS module
+ change in master MTLS Mode from Permissive to Strict
+ adding storage encrypted, and changing db engine to we
* **pbi542295:** + terraform version was changed from 0.12.29 to 1.0.10
+ implementing the possibility to use spot instances in EKS module
+ change in master MTLS Mode from Permissive to Strict
+ adding storage encrypted, and changing db engine to web

# [7.0.0-alpha.1](https://gitlab.com/study-gp/terragrunt-argocd/compare/v6.0.1-alpha.1...v7.0.0-alpha.1) (2022-02-28)


### Features

* **pbi542295:** edit README ([1b9405a](https://gitlab.com/study-gp/terragrunt-argocd/commit/1b9405aed2bbc595ecb36602c679ce027f751784))


### BREAKING CHANGES

* **pbi542295:** + terraform version was changed from 0.12.29 to 1.0.10
+ implementing the possibility to use spot instances in EKS module
+ change in master MTLS Mode from Permissive to Strict
+ adding storage encrypted, and changing db engine to we

## [6.0.1-alpha.1](https://gitlab.com/study-gp/terragrunt-argocd/compare/v6.0.0...v6.0.1-alpha.1) (2022-02-28)


### Bug Fixes

* **ci:** install @semantic-release/exec in the CI ([93c58c8](https://gitlab.com/study-gp/terragrunt-argocd/commit/93c58c85f7b4b525b25edd46708135ec9d7820d2))
* **ci:** typo in .releaserc file ([abe2b4a](https://gitlab.com/study-gp/terragrunt-argocd/commit/abe2b4adb9ea48b584f368c7d3a343f50cd33e61))


### Documentation

* **pbi542295:** edit README ([e9c9e94](https://gitlab.com/study-gp/terragrunt-argocd/commit/e9c9e94570d832da9da9673a4d7fb4c733294f34))


### BREAKING CHANGES

* **pbi542295:** + terraform version was changed from 0.12.29 to 1.0.10
+ implementing the possibility to use spot instances in EKS module
+ change in master MTLS Mode from Permissive to Strict
+ adding storage encrypted, and changing db engine to web

# [6.0.0](https://gitlab.com/study-gp/terragrunt-argocd/compare/v5.3.0...v6.0.0) (2022-02-16)


### Features

* **README:** some text ([9762ce6](https://gitlab.com/study-gp/terragrunt-argocd/commit/9762ce649c92e7a90205b24de00cb5edb9878599))


### BREAKING CHANGES

* **README:** you need to do next to upgrade

# [5.3.0](https://gitlab.com/study-gp/terragrunt-argocd/compare/v5.2.0...v5.3.0) (2022-02-16)


### Features

* **README:** some information - BREAKING CHANGE ([6ae529e](https://gitlab.com/study-gp/terragrunt-argocd/commit/6ae529e839864f1492a7028b28357f7880031e6f))

# [5.2.0](https://gitlab.com/study-gp/terragrunt-argocd/compare/v5.1.0...v5.2.0) (2022-02-09)


### Bug Fixes

* **pbi21212:** change source path for terraform module ([9cc560b](https://gitlab.com/study-gp/terragrunt-argocd/commit/9cc560b9575321676e2d04e56ecb08ae166c560f))
* **release:** add beta branch to the release job ([dc6a086](https://gitlab.com/study-gp/terragrunt-argocd/commit/dc6a086b6e4366d65e39f65bae8774846cf49fda))


### Features

* **pbi343434:** add new job to the GitLab-ci ([6bb8f9a](https://gitlab.com/study-gp/terragrunt-argocd/commit/6bb8f9a12cc4300e96db21c05158322b66ef2f78))

# [5.2.0-beta.1](https://gitlab.com/study-gp/terragrunt-argocd/compare/v5.1.0...v5.2.0-beta.1) (2022-02-09)


### Bug Fixes

* **pbi21212:** change source path for terraform module ([9cc560b](https://gitlab.com/study-gp/terragrunt-argocd/commit/9cc560b9575321676e2d04e56ecb08ae166c560f))
* **release:** add beta branch to the release job ([dc6a086](https://gitlab.com/study-gp/terragrunt-argocd/commit/dc6a086b6e4366d65e39f65bae8774846cf49fda))


### Features

* **pbi343434:** add new job to the GitLab-ci ([6bb8f9a](https://gitlab.com/study-gp/terragrunt-argocd/commit/6bb8f9a12cc4300e96db21c05158322b66ef2f78))

# [5.2.0-alpha.3](https://gitlab.com/study-gp/terragrunt-argocd/compare/v5.2.0-alpha.2...v5.2.0-alpha.3) (2022-02-09)


### Bug Fixes

* **release:** add beta branch to the release job ([dc6a086](https://gitlab.com/study-gp/terragrunt-argocd/commit/dc6a086b6e4366d65e39f65bae8774846cf49fda))

# [5.2.0-alpha.2](https://gitlab.com/study-gp/terragrunt-argocd/compare/v5.2.0-alpha.1...v5.2.0-alpha.2) (2022-02-09)


### Bug Fixes

* **pbi21212:** change source path for terraform module ([9cc560b](https://gitlab.com/study-gp/terragrunt-argocd/commit/9cc560b9575321676e2d04e56ecb08ae166c560f))

# [5.2.0-alpha.1](https://gitlab.com/study-gp/terragrunt-argocd/compare/v5.1.0...v5.2.0-alpha.1) (2022-02-09)


### Features

* **pbi343434:** add new job to the GitLab-ci ([6bb8f9a](https://gitlab.com/study-gp/terragrunt-argocd/commit/6bb8f9a12cc4300e96db21c05158322b66ef2f78))

## [2.1.1](https://gitlab.com/study-gp/terragrunt-argocd/compare/v2.1.0...v2.1.1) (2021-08-26)


### Bug Fixes

* **pbi000062:** another fix ([3bd344e](https://gitlab.com/study-gp/terragrunt-argocd/commit/3bd344ee87d51004f344e9a8e675e1550f5a66a3))

# [2.1.0](https://gitlab.com/study-gp/terragrunt-argocd/compare/v2.0.0...v2.1.0) (2021-08-26)


### Bug Fixes

* **merge:** resolve merge conflicts ([029b313](https://gitlab.com/study-gp/terragrunt-argocd/commit/029b31384b1cb026f55e6aa9aa87823fc33a9979))
* **merge:** resolve merge conflicts ([6cd6d83](https://gitlab.com/study-gp/terragrunt-argocd/commit/6cd6d83c17ade589af1d67974bff0821c10f7ef4))
* **pbi000032:** very small fix ([d783b4b](https://gitlab.com/study-gp/terragrunt-argocd/commit/d783b4be08676a4687b74489ce3778936f16a80f))
* **pbi00034:** some fix ([03afaac](https://gitlab.com/study-gp/terragrunt-argocd/commit/03afaac0fa56744445604c9896c5a951af634eaa))


### Features

* **pbi000023:** add new feature ([1214634](https://gitlab.com/study-gp/terragrunt-argocd/commit/121463492660401b3410b9b4c15fc3f75a8c28ec))

# [2.0.0](https://gitlab.com/study-gp/terragrunt-argocd/compare/v1.4.0...v2.0.0) (2021-08-26)


### Features

* **pbi00013:** one more test ([b48e2d2](https://gitlab.com/study-gp/terragrunt-argocd/commit/b48e2d24bb425928310498aa3e8e715f452c3d9a))


### BREAKING CHANGES

* **pbi00013:** some text here

# [1.4.0](https://gitlab.com/study-gp/terragrunt-argocd/compare/v1.3.0...v1.4.0) (2021-08-26)


### Features

* **pbi00012:** one more test ([771f6e5](https://gitlab.com/study-gp/terragrunt-argocd/commit/771f6e5a7b8c553e74fdcb605203a3e330de6302))


### BREAKING CHANGES

* **pbi00012:** some text here

# [1.3.0](https://gitlab.com/study-gp/terragrunt-argocd/compare/v1.2.0...v1.3.0) (2021-08-26)


### Features

* **pbi00012:** one more test ([5ac64f5](https://gitlab.com/study-gp/terragrunt-argocd/commit/5ac64f5570af4415a59a8c18a8c0eed7c6ba37b2))


### BREAKING CHANGES

* **pbi00012:** changes

# [1.2.0](https://gitlab.com/study-gp/terragrunt-argocd/compare/v1.1.0...v1.2.0) (2021-08-26)


### Features

* **pbi00012:** upgrade aks module ([791a802](https://gitlab.com/study-gp/terragrunt-argocd/commit/791a802258ac482b813d3b6ba88c38216231c5ef))
* **pbi00021:** use new function from aks ([889ab8a](https://gitlab.com/study-gp/terragrunt-argocd/commit/889ab8a436e84463ac1b4859f9b9b724f92af040))


### BREAKING CHANGES

* **pbi00012:** upgrade to new version
some text here

# [1.1.0](https://gitlab.com/study-gp/terragrunt-argocd/compare/v1.0.0...v1.1.0) (2021-08-26)


### Features

* **pbi00004:** add some feature ([48fa807](https://gitlab.com/study-gp/terragrunt-argocd/commit/48fa80707a2d0ed04bf548d1a16cb7f9a83bd87e))

# 1.0.0 (2021-08-26)


### Continuous Integration

* **pbi000012:** add ci config for this repo ([200a6af](https://gitlab.com/study-gp/terragrunt-argocd/commit/200a6afe15ab110ae3e05b096cb990cecff06bd5))


### BREAKING CHANGES

* **pbi000012:** this repo will be update its version and create release with Changelog
